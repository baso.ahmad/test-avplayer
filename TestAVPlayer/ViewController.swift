//
//  ViewController.swift
//  TestAVPlayer
//
//  Created by Ruangguru on 13/05/22.
//

import UIKit
import AVKit

class ViewController: UIViewController {
    
    let videoURL = URL(string: "https://vod-stag.akamaized.net/agora/0861fe8260e04898835f85fc11497f2c/master.m3u8")!
    
    lazy var player = AVPlayer(url: videoURL)
    
    var observer: NSKeyValueObservation?
    
    deinit {
        observer?.invalidate()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        playVideo()
        observeError()
    }

    
    func playVideo() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            let playerViewController = AVPlayerViewController()
            playerViewController.player = self.player
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }
        }
    }
    
    func observeError() {
        observer = player.currentItem?.observe(\.error, options: .new) { _, error in
            guard let error = error.newValue else { return }
            print("error : ", error)
        }
    }

}

